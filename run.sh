#!/bin/sh
if [ ! -d "nwjs" ]; then
    wget http://bychawski.net/downloads/nwjs-v0.12.1-linux-x64.tar.gz
    tar -xf nwjs-v0.12.1-linux-x64.tar.gz
    mv nwjs-v0.12.1-linux-x64 nwjs
    rm nwjs-v0.12.1-linux-x64.tar.gz
fi
./nwjs/nw --ignore-certificate-errors .