window.MainView = (function() {
    return Backbone.View.extend({
        tagName    : 'div',

        initialize : function(options) {
            var self = this;
            this.render();
        },

        render : function() {
            var self = this;
            this.$el.html( templates.mainView() );
            return this;
        }
    });
})();